
const express = require('express')
const axios = require('axios')
const parser = require('body-parser')
const app = express()
const port = 8000
const { posts } = require('./endpoints')
const { authenticate } = require('./middlewares')

// parse application/x-www-form-urlencoded
app.use(parser.urlencoded({extended: false}))

// parse application/json
app.use(parser.json())

const postsHandler = posts({ axios })
app.post('/', authenticate, postsHandler.post)

app.listen(port, () => {
  console.log(`App listening on port ${port}!`)
})
