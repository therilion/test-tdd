const authenticate = require('./authenticate')
describe('Middlewares', () => {
  describe('Authenticate', () => {
    it('Should have id 1', () => {
      // Definir la inyeccion de parámetros
      const req = {
        header: jest.fn().mockReturnValue(1)
      }

      const res = {
        sendStatus: jest.fn()
      }

      const next = jest.fn()
      
      // Llamado al método a testear
      authenticate(req, res, next)

      // Espera a recibir el header el header user_id
      expect(req.header.mock.calls).toEqual([
        ['user_id']
      ])
      // Espera a que sendStatus NO sea llamado
      expect(res.sendStatus.mock.calls).toEqual([])
      // Espera a que next si se haya llamado, sin argumentos
      expect(next.mock.calls).toEqual([[]])
    })

    it('Should fail if user is not the one with id 1', () => {
      // Definir la inyeccion de parámetros
      const req = {
        header: jest.fn().mockReturnValue(2)
      }

      const res = {
        sendStatus: jest.fn()
      }

      const next = jest.fn()
      
      // Llamado al método a testear
      authenticate(req, res, next)

      // Espera a recibir el header el header user_id
      expect(req.header.mock.calls).toEqual([
        ['user_id']
      ])
      // Espera a que sendStatus sea llamado con el parámetro 403
      expect(res.sendStatus.mock.calls).toEqual([
        [403]
      ])
      // Espera a que next NO se haya ejecutado
      expect(next.mock.calls).toEqual([])
    })
  })
})